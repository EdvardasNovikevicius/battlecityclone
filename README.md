# README #

### How do I get set up? ###

### If build settings have been resetted ###

After cloning/downloading repository follow this steps to set up a project:

* 1. Open project with unity 2017.1.03f or greater
* 2. Go to File->Build Settings... 
* 3. Drag and drop MainMenu.unity from _Scenes folder to "Scenes in Build"
* 4. Drag and drop Game.unity from _Scenes folder to "Scenes in Build"
* 5. Close Build Settings window

### If bullets explosion animation is not playing (bullets are destoryed only after few seconds they have collied with any object) ###

* 1. Open project with unity 2017.1.03f or greater
* 2. Go to Animation -> Controllers folder
* 3. Double click Bullet animator
* 4. Click on BulletExplosion in the Animator window
* 5. Go back to Anmation folder
* 6. Drag and drop BulletExplosion.anim from Anmation folder to Inspector -> Motion 
* 7. Save scene and click play

Edvardas Novikevičius (edvardas.novikevicius@gmail.com)